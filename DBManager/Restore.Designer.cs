﻿namespace DBManager
{
    partial class Restore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clbDbList = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gvFileList = new System.Windows.Forms.DataGridView();
            this.btnRestore = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gvFileList)).BeginInit();
            this.SuspendLayout();
            // 
            // clbDbList
            // 
            this.clbDbList.CheckOnClick = true;
            this.clbDbList.FormattingEnabled = true;
            this.clbDbList.Location = new System.Drawing.Point(13, 29);
            this.clbDbList.Name = "clbDbList";
            this.clbDbList.Size = new System.Drawing.Size(130, 196);
            this.clbDbList.TabIndex = 0;
            this.clbDbList.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbDbList_ItemCheck);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "数据库列表(单选)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(155, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "历史备份列表";
            // 
            // gvFileList
            // 
            this.gvFileList.AllowUserToAddRows = false;
            this.gvFileList.AllowUserToDeleteRows = false;
            this.gvFileList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvFileList.Location = new System.Drawing.Point(157, 29);
            this.gvFileList.Name = "gvFileList";
            this.gvFileList.ReadOnly = true;
            this.gvFileList.RowTemplate.Height = 23;
            this.gvFileList.Size = new System.Drawing.Size(281, 196);
            this.gvFileList.TabIndex = 3;
            // 
            // btnRestore
            // 
            this.btnRestore.Location = new System.Drawing.Point(157, 246);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(126, 23);
            this.btnRestore.TabIndex = 4;
            this.btnRestore.Text = "还原所选备份文件";
            this.btnRestore.UseVisualStyleBackColor = true;
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // Restore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 281);
            this.Controls.Add(this.btnRestore);
            this.Controls.Add(this.gvFileList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.clbDbList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Restore";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "数据库还原";
            this.Load += new System.EventHandler(this.Restore_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gvFileList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox clbDbList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView gvFileList;
        private System.Windows.Forms.Button btnRestore;
    }
}