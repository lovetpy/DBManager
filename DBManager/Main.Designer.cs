﻿namespace DBManager
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtServerName = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDbList = new System.Windows.Forms.Button();
            this.btnClearData = new System.Windows.Forms.Button();
            this.btnClearData1 = new System.Windows.Forms.Button();
            this.btnClearData2 = new System.Windows.Forms.Button();
            this.btnShowRestoreForm = new System.Windows.Forms.Button();
            this.btnBackupShow = new System.Windows.Forms.Button();
            this.btnConnDB = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tvDBList = new System.Windows.Forms.MyTreeView();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "服务器";
            // 
            // txtServerName
            // 
            this.txtServerName.Location = new System.Drawing.Point(65, 19);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.Size = new System.Drawing.Size(98, 21);
            this.txtServerName.TabIndex = 1;
            this.txtServerName.Text = "localhost";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(212, 19);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(47, 21);
            this.txtUserName.TabIndex = 3;
            this.txtUserName.Text = "sa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(168, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "用户名";
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new System.Drawing.Point(297, 19);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.Size = new System.Drawing.Size(103, 21);
            this.txtPwd.TabIndex = 5;
            this.txtPwd.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(264, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "密码";
            // 
            // btnDbList
            // 
            this.btnDbList.Location = new System.Drawing.Point(232, 90);
            this.btnDbList.Name = "btnDbList";
            this.btnDbList.Size = new System.Drawing.Size(168, 23);
            this.btnDbList.TabIndex = 6;
            this.btnDbList.Text = "查看数据库列表";
            this.btnDbList.UseVisualStyleBackColor = true;
            this.btnDbList.Click += new System.EventHandler(this.btnDbList_Click);
            // 
            // btnClearData
            // 
            this.btnClearData.Location = new System.Drawing.Point(232, 127);
            this.btnClearData.Name = "btnClearData";
            this.btnClearData.Size = new System.Drawing.Size(168, 23);
            this.btnClearData.TabIndex = 8;
            this.btnClearData.Text = "删除左侧所选表内数据";
            this.btnClearData.UseVisualStyleBackColor = true;
            this.btnClearData.Click += new System.EventHandler(this.btnClearData_Click);
            // 
            // btnClearData1
            // 
            this.btnClearData1.Location = new System.Drawing.Point(232, 164);
            this.btnClearData1.Name = "btnClearData1";
            this.btnClearData1.Size = new System.Drawing.Size(168, 23);
            this.btnClearData1.TabIndex = 14;
            this.btnClearData1.Text = "删除数据1(代理+玩家+记录)";
            this.btnClearData1.UseVisualStyleBackColor = true;
            this.btnClearData1.Click += new System.EventHandler(this.btnClearData1_Click);
            // 
            // btnClearData2
            // 
            this.btnClearData2.Location = new System.Drawing.Point(232, 201);
            this.btnClearData2.Name = "btnClearData2";
            this.btnClearData2.Size = new System.Drawing.Size(168, 23);
            this.btnClearData2.TabIndex = 15;
            this.btnClearData2.Text = "删除数据2(预留玩家+代理)";
            this.btnClearData2.UseVisualStyleBackColor = true;
            this.btnClearData2.Click += new System.EventHandler(this.btnClearData2_Click);
            // 
            // btnShowRestoreForm
            // 
            this.btnShowRestoreForm.Location = new System.Drawing.Point(232, 275);
            this.btnShowRestoreForm.Name = "btnShowRestoreForm";
            this.btnShowRestoreForm.Size = new System.Drawing.Size(168, 23);
            this.btnShowRestoreForm.TabIndex = 16;
            this.btnShowRestoreForm.Text = "数据库还原";
            this.btnShowRestoreForm.UseVisualStyleBackColor = true;
            this.btnShowRestoreForm.Click += new System.EventHandler(this.btnShowRestoreForm_Click);
            // 
            // btnBackupShow
            // 
            this.btnBackupShow.Location = new System.Drawing.Point(232, 238);
            this.btnBackupShow.Name = "btnBackupShow";
            this.btnBackupShow.Size = new System.Drawing.Size(168, 23);
            this.btnBackupShow.TabIndex = 17;
            this.btnBackupShow.Text = "数据库备份";
            this.btnBackupShow.UseVisualStyleBackColor = true;
            this.btnBackupShow.Click += new System.EventHandler(this.btnBackupShow_Click);
            // 
            // btnConnDB
            // 
            this.btnConnDB.Location = new System.Drawing.Point(232, 53);
            this.btnConnDB.Name = "btnConnDB";
            this.btnConnDB.Size = new System.Drawing.Size(168, 23);
            this.btnConnDB.TabIndex = 18;
            this.btnConnDB.Text = "数据库连接";
            this.btnConnDB.UseVisualStyleBackColor = true;
            this.btnConnDB.Click += new System.EventHandler(this.btnConnDB_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 12);
            this.label4.TabIndex = 19;
            this.label4.Text = "服务器数据库列表";
            // 
            // tvDBList
            // 
            this.tvDBList.CheckBoxes = true;
            this.tvDBList.Location = new System.Drawing.Point(23, 68);
            this.tvDBList.Name = "tvDBList";
            this.tvDBList.Size = new System.Drawing.Size(186, 232);
            this.tvDBList.TabIndex = 7;
            this.tvDBList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tvDBList_MouseClick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 312);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnConnDB);
            this.Controls.Add(this.btnBackupShow);
            this.Controls.Add(this.btnShowRestoreForm);
            this.Controls.Add(this.btnClearData2);
            this.Controls.Add(this.btnClearData1);
            this.Controls.Add(this.btnClearData);
            this.Controls.Add(this.tvDBList);
            this.Controls.Add(this.btnDbList);
            this.Controls.Add(this.txtPwd);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtServerName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SQL数据管理助手 V2.2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtServerName;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDbList;
        private System.Windows.Forms.MyTreeView tvDBList;
        private System.Windows.Forms.Button btnClearData;
        private System.Windows.Forms.Button btnClearData1;
        private System.Windows.Forms.Button btnClearData2;
        private System.Windows.Forms.Button btnShowRestoreForm;
        private System.Windows.Forms.Button btnBackupShow;
        private System.Windows.Forms.Button btnConnDB;
        private System.Windows.Forms.Label label4;
    }
}

