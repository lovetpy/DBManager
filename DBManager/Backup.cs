﻿using DBManager.Helper;
using DBManager.UserCode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBManager
{
    public partial class Backup : Form
    {
        public Backup(string serverName, string userName, string userPwd)
        {
            InitializeComponent();
            this.Icon = MyResource.icon;
            this.serverName = serverName;
            this.userName = userName;
            this.userPwd = userPwd;

            lbBackupPath.Text += FileCommon.backupPath;
        }

        private string serverName { get; set; }

        private string userName { get; set; }

        private string userPwd { get; set; }
        
        /// <summary>
        /// 获取连接字符串
        /// </summary>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public string GetConnStr(string dbName = "master")
        {
            return SqlCommon.GetConnStr(serverName, userName, userPwd, dbName);
        }

        private void Backup_Load(object sender, EventArgs e)
        {
            DataTable dt = SqlCommon.GetDataBase(GetConnStr());
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    clbDbList.Items.Add(dr["name"]);
                }
            }
        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            if (clbDbList.CheckedItems.Count > 0)
            {
                btnBackup.Enabled = false;
                btnBackup.Text = "数据库备份中....";
                clbDbList.Enabled = false;
                string connStr = GetConnStr();
                Task task = new Task(() =>
                {
                    //FileHelper.CreateDirectory(FileCommon.backupPath);
                    StringBuilder sbStr = new StringBuilder();
                    //创建备份文件夹
                    sbStr.Append("exec xp_create_subdir '" + FileCommon.backupPath + "';");

                    foreach (object item in clbDbList.CheckedItems)
                    {
                        sbStr.Append("BACKUP DATABASE ");
                        sbStr.Append(item.ToString());
                        sbStr.Append(" TO DISK = '");
                        sbStr.Append(FileCommon.backupPath);
                        sbStr.Append(DateTime.Now.ToString("yyyyMMddHHmmss"));
                        sbStr.Append("_");
                        sbStr.Append(item.ToString());
                        sbStr.Append(".bak' WITH INIT;");
                    }
                    using (SqlConnection conn = new SqlConnection(connStr))
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(sbStr.ToString(), conn);
                        cmd.CommandTimeout = 300;
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("备份成功！");
                    }

                    btnBackup.Enabled = true;
                    btnBackup.Text = "一键备份所选数据库";
                    clbDbList.Enabled = true;
                });
                task.Start();
            }
            else
                MessageBox.Show("请选择需要备份的数据库！", "提示");
        }
    }
}
