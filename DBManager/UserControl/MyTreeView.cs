﻿using System.Windows.Forms;

namespace System.Windows.Forms
{
    class MyTreeView : TreeView
    {
        protected override void WndProc(ref Message m)
        {
            if (m.Msg != 0x203)
            {
                base.WndProc(ref m);
            }
        }
    }
}
