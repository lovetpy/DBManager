﻿namespace DBManager
{
    partial class Backup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clbDbList = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBackup = new System.Windows.Forms.Button();
            this.lbBackupPath = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // clbDbList
            // 
            this.clbDbList.CheckOnClick = true;
            this.clbDbList.FormattingEnabled = true;
            this.clbDbList.Location = new System.Drawing.Point(12, 31);
            this.clbDbList.Name = "clbDbList";
            this.clbDbList.Size = new System.Drawing.Size(213, 180);
            this.clbDbList.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "数据库列表(可多选)";
            // 
            // btnBackup
            // 
            this.btnBackup.Location = new System.Drawing.Point(12, 221);
            this.btnBackup.Name = "btnBackup";
            this.btnBackup.Size = new System.Drawing.Size(213, 23);
            this.btnBackup.TabIndex = 3;
            this.btnBackup.Text = "一键备份所选数据库";
            this.btnBackup.UseVisualStyleBackColor = true;
            this.btnBackup.Click += new System.EventHandler(this.btnBackup_Click);
            // 
            // lbBackupPath
            // 
            this.lbBackupPath.AutoSize = true;
            this.lbBackupPath.ForeColor = System.Drawing.Color.Red;
            this.lbBackupPath.Location = new System.Drawing.Point(12, 253);
            this.lbBackupPath.Name = "lbBackupPath";
            this.lbBackupPath.Size = new System.Drawing.Size(125, 12);
            this.lbBackupPath.TabIndex = 4;
            this.lbBackupPath.Text = "备份目录（服务器）：";
            this.lbBackupPath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Backup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(237, 272);
            this.Controls.Add(this.lbBackupPath);
            this.Controls.Add(this.btnBackup);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.clbDbList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Backup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "数据库备份";
            this.Load += new System.EventHandler(this.Backup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox clbDbList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBackup;
        private System.Windows.Forms.Label lbBackupPath;
    }
}