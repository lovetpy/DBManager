﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBManager.UserCode
{
    public class ClearConfig
    {
        public static string GetClearStr(bool isAll)
        {
            StringBuilder sb = new StringBuilder();
            if (isAll)
            {
                //代理数据
                sb.Append("delete FROM [CXAgentDB].[dbo].[Agent];");
                //代理抽水设定
                sb.Append("delete FROM [CXAgentDB].[dbo].[AgentRevRate];");
                //玩家信息表
                sb.Append("delete FROM [CXGameUserDB].[dbo].[AccountsInfo];");
                //玩家详细信息
                sb.Append("delete FROM [CXGameUserDB].[dbo].[IndividualDatum];");
            }

            //机器人
            sb.Append("DELETE FROM [CXGameUserDB].[dbo].[AccountsInfo] WHERE USERID IN(SELECT USERID FROM [CXTreasureDB].[dbo].[AndroidUserInfo]);");
            sb.Append("delete FROM [CXTreasureDB].[dbo].[AndroidUserInfo];");

            //代理金币变化记录
            sb.Append("delete FROM [CXAgentDB].[dbo].[AgentNewScoreChange];");
            sb.Append("delete FROM [CXAgentDB].[dbo].[RvenceTempLog];");
            //登录登出次数
            sb.Append("delete FROM [CXTreasureDB].[dbo].[SystemStreamInfo];");
            sb.Append("delete FROM [CXTreasureDB].[dbo].[TempMatch];");
            sb.Append("delete FROM [CXTreasureDB].[dbo].[TypeScoreLog];");
            sb.Append("delete FROM [CXTreasureDB].[dbo].[UserAideRight];");
            sb.Append("delete FROM [CXTreasureDB].[dbo].[YB_DemoRoomRecord];");
            sb.Append("delete FROM [CXTreasureDB].[dbo].[UserCheatList];");
            sb.Append("delete FROM [CXTreasureDB].[dbo].[Trade];");
            sb.Append("delete FROM [CXTreasureDB].[dbo].[TaskUserPay];");
            sb.Append("delete FROM [CXTreasureDB].[dbo].[TaskPaiXingLog];");
            sb.Append("delete FROM [CXAgentDB].[dbo].[AgentApplyLog];");
            sb.Append("delete FROM [CXAgentDB].[dbo].[AgentChangeLog];");
            //IP地址限制
            sb.Append("delete FROM [CXGameUserDB].[dbo].[ConfineAddress];");
            sb.Append("delete FROM [CXGameUserDB].[dbo].[ConfineContent];");
            sb.Append("delete FROM [CXGameUserDB].[dbo].[ConfineMachine];");
            sb.Append("delete FROM [CXGameUserDB].[dbo].[GlobalInfo];");
            sb.Append("delete FROM [CXGameUserDB].[dbo].[RecordMACCount];");
            //成功次数
            sb.Append("delete FROM [CXGameUserDB].[dbo].[SystemStreamInfo];");
            sb.Append("delete FROM [CXGameUserDB].[dbo].[YB_BindingActive];");
            sb.Append("delete FROM [CXMessageDB].[dbo].[AdminList];");
            //游戏抽水详情
            sb.Append("delete FROM [CXTreasureDB].[dbo].[GameScoreInfo];");
            sb.Append("delete FROM [CXTreasureDB].[dbo].[GameScoreLocker];");

            //代理抽水设定
            sb.Append("delete FROM [CXAgentDB].[dbo].[ChangeScoreRecord];");
            sb.Append("delete FROM [CXAgentDB].[dbo].[EveryDay_ScoreStatistics];");

            //登录日志
            sb.Append("delete FROM [CXTreasureDB].[dbo].[LogonLog];");
            sb.Append("delete FROM [CXTreasureDB].[dbo].[OnLineOrder];");

            //操作日志
            sb.Append("delete FROM [CXTreasureDB].[dbo].[OperateLog];");

            //游戏记录
            sb.Append("delete FROM [CXTreasureDB].[dbo].[RecordUserGameInfo];");

            //金币变化
            sb.Append("delete FROM [CXTreasureDB].[dbo].[ScoreLog];");
            sb.Append("delete FROM [CXTreasureDB].[dbo].[ShareDetailInfo];");


            //修改登录密码记录
            sb.Append("delete FROM [CXGameUserDB].[dbo].[RecordUserChangePassword];");

            //修改银行记录
            sb.Append("delete FROM [CXGameUserDB].[dbo].[RecordUserChangeBankInfo];");

            //银行卡信息
            sb.Append("delete FROM [CXTreasureDB].[dbo].[BankCardInfo];");
            return sb.ToString();
        }
    }
}