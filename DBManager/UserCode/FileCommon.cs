﻿using DBManager.Helper;
using DBManager.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBManager.UserCode
{
    /// <summary>
    /// 文件操作通用类
    /// </summary>
    public static class FileCommon
    {

        public static string backupPath = "C:\\beifenku\\";
        public static string xmlFilePath = "ClearConfig.xml";

        /// <summary>
        /// 获取路径下文件信息
        /// </summary>
        /// <param name="directoryPath">指定目录的绝对路径</param>
        /// <param name="searchPattern">模式字符串，"*"代表0或N个字符，"?"代表1个字符。
        /// 范例："Log*.xml"表示搜索所有以Log开头的Xml文件。</param>
        /// <returns></returns>
        public static List<FileInfoModel> GetFileInfoList(string directoryPath, string searchPattern)
        {
            List<FileInfoModel> list = new List<FileInfoModel>();
            string[] files = FileHelper.GetFileNames(directoryPath, searchPattern, false);
            foreach (string item in files)
            {
                FileInfoModel model = new FileInfoModel();
                FileInfo file =new FileInfo(item);
                model.FileName = file.Name;
                model.CreateTime = file.CreationTime.ToString("yyyy-MM-dd HH:mm:ss");
                model.FileSize = Math.Round(file.Length / 1024.00).ToString() + "kb";
                list.Add(model);
            }
            return list;
        }
    }
}
