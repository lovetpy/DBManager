﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBManager.Model
{
    public class FileInfoModel
    {
        public string FileName { get; set; }

        public string CreateTime { get; set; }

        public string FileSize { get; set; }
    }
}
